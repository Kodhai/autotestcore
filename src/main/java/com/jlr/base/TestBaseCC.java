package com.jlr.base;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.jlr.autotest.constants.Browsers;
import com.jlr.autotest.constants.CommonConstants;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;

public class TestBaseCC extends DriverManager {

	Browsers browser = Browsers.CHROME;
	private static final Logger LOGGER = LoggerFactory.getLogger(TestBaseCC.class);
	public static ExtentReports extentReports;
	public static final ThreadLocal<ExtentTest> extentLogger = new ThreadLocal<>();

	public void setupTest(String test) {

		if (null != System.getenv("SELENIUM_BROWSER")) {
			browser = Browsers.getBrowser(System.getenv("SELENIUM_BROWSER"));
			LOGGER.info("Executing tests on " + browser.toString() + " browser...");
		}
		configReport(); // 1
		loggerConfig(test); // 2
		configureBrowser(browser);
	}

	protected RemoteWebDriver createChromeDriver(Browsers browser) {

		System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("test-type");
		options.addArguments("disable-infobars");
		options.addArguments("–lang=it");
		options.setExperimentalOption("useAutomationExtension", false);
		Map<String, Object> prefs = new HashMap<String, Object>();
		// prefs.put("download.prompt_for_download",false);
		// prefs.put("download.default_directory",System.getProperty("user.dir")+"/Downloads/");

		prefs.put("profile.default_content_settings.popups", 0);
		prefs.put("download.default_directory", CommonConstants.FILE_PATH);

		options.setExperimentalOption("prefs", prefs);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();

		capabilities.setCapability("chromeOptions", options);
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		if (Utility.isWindowsStylePath()) {
			LOGGER.info("Platform being used for execution is Windows");
		} else {
			LOGGER.info("Platform being used for execution is Linux");
		}

		RemoteWebDriver driver = null;
		try {
			ActionHandler.wait(13);
			 driver = new ChromeDriver(capabilities);		
			//driver = new RemoteWebDriver(new URL("http://localhost:49044/wd/hub"), capabilities);
			/*String[] bat = { "cmd.exe", "/C", "Start", Utility.fetchHomeDir() + "\\GridStart.bat" };
			Runtime.getRuntime().exec(bat);*/
		
		} catch (Exception e) {
			LOGGER.error("Exception occured :", e);
		}

		return driver;

	}

	protected RemoteWebDriver createIEDriver(Browsers browser) {

		System.setProperty("webdriver.ie.driver", "C:\\Jars\\IEDriverServer.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		/*
		 * capabilities.setCapability(InternetExplorerDriver. IE_ENSURE_CLEAN_SESSION,
		 * true);
		 */
		capabilities.setCapability("ignoreProtectedModeSettings", true);

		RemoteWebDriver driver = null;
		try {
			// driver = new InternetExplorerDriver(capabilities);
			driver = new RemoteWebDriver(new URL("http://localhost:49044/wd/hub"), capabilities);
			driver.manage().window().maximize();
		} catch (Exception e) {
			LOGGER.error("Exception occured :", e);
		}
		return driver;
	}

	public void configReport() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.getProperty("user.dir"));
		sb.append("/test-output/extentReport");
		// File file = new
		// File(System.getProperty("user.dir")+"/test-output/extentReport/extent-Report.html");
		File file = new File(sb.toString());
		if (!file.exists()) {
			file.mkdirs();
		}
		ExtentHtmlReporter extentHtmlReporter = new ExtentHtmlReporter(sb.append("/extent-Report.html").toString());
		extentHtmlReporter
				.loadXMLConfig(new File(System.getProperty("user.dir") + "/src/test/resources/extent-config.xml"));
		extentReports = new ExtentReports();

		extentReports.attachReporter(extentHtmlReporter);

	}

	public static ExtentTest getextentLogger() {
		return extentLogger.get();
	}

	public void setextentLogger(ExtentTest exLogger) {
		extentLogger.set(exLogger);
	}

	private void loggerConfig(String testName) {
		ExtentTest exLogger = extentReports.createTest(testName);
		exLogger.log(Status.INFO, "starting test " + testName + "");
		setextentLogger(exLogger);
	}
}
