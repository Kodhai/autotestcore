package com.jlr.base;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.jlr.autotest.constants.Browsers;
import com.jlr.autotest.utils.Utility;

public class TestBase extends DriverManager {

	Browsers browser = Browsers.CHROME;
	private static final Logger LOGGER = LoggerFactory.getLogger(TestBase.class);
	public static ExtentReports extentReports;
	public static final ThreadLocal<ExtentTest> extentLogger = new ThreadLocal<>();

	@BeforeSuite
	public void start() {
		onStart();
		configReport();
	}

	@AfterSuite
	public void end() {
		onFinish();
		extentReports.flush();
	}

	public void setupTest(String testName) throws MalformedURLException {

		if (null != System.getenv("SELENIUM_BROWSER")) {
			browser = Browsers.getBrowser(System.getenv("SELENIUM_BROWSER"));
			LOGGER.info("Executing tests on " + browser.toString() + " browser...");
		}
		loggerConfig(testName);
		configureBrowser(browser);
	}

	protected WebDriver createChromeDriver(Browsers browser) {

		System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("test-type");
		options.addArguments("disable-infobars");
		//To Disable pdf viewer plugin
/*		Map<String,Object> prefs = new HashMap<>();
		prefs.put("plugins.always_open_pdf_externally", true);
		options.setExperimentalOption("prefs", prefs);*/
		
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();

		capabilities.setCapability("chromeOptions", options);
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		if (Utility.isWindowsStylePath()) {
			LOGGER.info("Platform being used for execution is Windows");
		} else {
			LOGGER.info("Platform being used for execution is Linux");
		}

		WebDriver driver = null;
		try {
			driver=new ChromeDriver();
			//driver = new RemoteWebDriver(new URL("http://localhost:49044/wd/hub"), capabilities);
		} /*catch (MalformedURLException e) {
			LOGGER.error("MalformedURL Exception occured: ", e);
		}*/
		catch (Exception e) {
			System.out.println(e);
		}

		return driver;

	}

	protected RemoteWebDriver createIEDriver(Browsers browser) {

		System.setProperty("webdriver.ie.driver", "C:\\Jars\\IEDriverServer.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		/*
		 * capabilities.setCapability(InternetExplorerDriver.
		 * IE_ENSURE_CLEAN_SESSION, true);
		 */
		capabilities.setCapability("ignoreProtectedModeSettings", true);
		// capabilities.setCapability("browserName", browser.toString());

		RemoteWebDriver driver = null;
		try {
			driver = new RemoteWebDriver(new URL("http://localhost:49044/wd/hub"), capabilities);
			driver.manage().window().maximize();
		} catch (MalformedURLException e) {
			LOGGER.error("MalformedURL Exception occured: ", e);
		}
		return driver;
	}

	public void configReport() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.getProperty("user.dir"));
		sb.append("/test-output/extentReport");
		// File file = new
		// File(System.getProperty("user.dir")+"/test-output/extentReport/extent-Report.html");
		File file = new File(sb.toString());
		if (!file.exists()) {
			file.mkdirs();
		}
		ExtentHtmlReporter extentHtmlReporter = new ExtentHtmlReporter(sb.append("/extent-Report.html").toString());
		extentHtmlReporter
				.loadXMLConfig(new File(System.getProperty("user.dir") + "/src/test/resources/extent-config.xml"));
		extentReports = new ExtentReports();

		extentReports.attachReporter(extentHtmlReporter);

	}

	public static ExtentTest getextentLogger() {
		return extentLogger.get();
	}

	public void setextentLogger(ExtentTest exLogger) {
		extentLogger.set(exLogger);
	}

	private void loggerConfig(String testName) {
		ExtentTest exLogger = extentReports.createTest(testName);
		exLogger.log(Status.INFO, "starting test " + testName + "");
		setextentLogger(exLogger);
	}

}
