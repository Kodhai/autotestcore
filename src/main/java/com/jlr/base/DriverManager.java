package com.jlr.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.jlr.autotest.constants.Browsers;
import com.jlr.listeners.Listener;

public abstract class DriverManager extends Listener {
	
	public static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();
	
		
	public void configureBrowser(Browsers browser) {
		//RemoteWebDriver dr = null;
		WebDriver dr = null;
		switch (browser) {

		case CHROME:
			dr = createChromeDriver(browser);
			break;

		case IE:
			dr = createIEDriver(browser);
			break;

		default:
			Assert.fail("BrowserFactory.getBrowser - Invalid browser selected.");
			break;

		}
		setWebDriver(dr);
	}
	
	//public abstract void setupTest(String testName) throws MalformedURLException;
	
	protected abstract WebDriver createChromeDriver(Browsers browser);
	
	protected abstract WebDriver createIEDriver(Browsers browser);
	
	public static WebDriver getDriver() {
		return driver.get();
	}

	public void setWebDriver(WebDriver dr) {
		driver.set(dr);
	}

}
