package com.jlr.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Utility;

public class Listener {

	private static final Logger LOGGER = LoggerFactory.getLogger(Listener.class);
	private boolean isOnStartFirstTime = true;

	public void onStart() {

		if (Utility.isWindowsStylePath() && isOnStartFirstTime) {
			try {
				String[] bat = { "cmd.exe", "/C", "Start", Utility.fetchHomeDir() + "\\GridStart.bat" };
			//	Runtime.getRuntime().exec(bat);
				ActionHandler.wait(10);
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("Selenium grid started... ");
				}
				isOnStartFirstTime = false;
			} catch (Exception e) {
				LOGGER.error("Exception while executing BAT command");
			}
		}
	}

	public void onFinish() {
		if (Utility.isWindowsStylePath()) {
			try {
				ActionHandler.wait(3);
				LOGGER.info("Killing all CMDs related to GRID...");
				//Runtime.getRuntime().exec("taskkill /F /IM cmd.exe /T");

				ActionHandler.wait(3);
				LOGGER.info("Killing all open process instances of Chrome Webdriver.");
				Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");

			} catch (Exception e1) {
				LOGGER.error("Exception occurred: ", e1);
			}
		}

	}

}
