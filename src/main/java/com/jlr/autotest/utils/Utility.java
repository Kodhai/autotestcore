package com.jlr.autotest.utils;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jlr.autotest.constants.CommonConstants;
import com.jlr.base.DriverManager;
import com.jlr.base.TestBase;

public class Utility {

	private static final Logger LOGGER = LoggerFactory.getLogger(Utility.class);

	public static int getDefaultWaitTime() {

		return CommonConstants.DEFAULTWAITTIME;
	}

	/**
	 * 
	 * @param value
	 */
	public static void copyToClipBoard(String value) {

		StringSelection selection = new StringSelection(value);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection, selection);
		try {
			Transferable transferable = clipboard.getContents(null);
			if (transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				String data = (String) transferable.getTransferData(DataFlavor.stringFlavor);
				if (StringUtils.isNotBlank(data)) {
					LOGGER.debug("Data successfully copied to clipboard with contents: " + data);
				} else {
					LOGGER.debug("Data copy to clipboard was unsuccessful.");
				}
			}
		} catch (UnsupportedFlavorException e) {
			LOGGER.error("Utility.copyToClipBoard - UnsupportedFlavorException oocured: " + e);
		} catch (IOException e) {
			LOGGER.error("Utility.copyToClipBoard - IOException oocured: " + e);
		}
	}

	public static void pasteDataFromClipBoard() {
		Actions actions = new Actions(TestBase.getDriver());
		actions.sendKeys(Keys.chord(Keys.LEFT_CONTROL, "v")).build().perform();
		LOGGER.debug("Data pasted from clipboard.");
	}

	public static final String takeScreenShot() {
		StringBuilder sb = new StringBuilder();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(CommonConstants.SCREENSHOTFORMAT);

			String str = fetchHomeDir().replace("\\", "/");
			sb.append(str);
			sb.append("/screenshots/");
			sb.append(sdf.format(Utility.fetchCurrentDate()));
			sb.append(CommonConstants.SCREENSHOTFILEEXTENSION);
			File source = ((TakesScreenshot) TestBase.getDriver()).getScreenshotAs(OutputType.FILE);
			LOGGER.debug("screenshot file: " + source + ", path -->" + sb.toString());

			FileUtils.copyFile(source, new File(sb.toString()));
		} catch (Exception ioException) {
			LOGGER.error("Utility.takeScreenShot - IOException occurred: ", ioException);
		}
		return sb.toString();
	}

	public static final String takeScreenShotWithPathName(String screenshotName) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append(screenshotName);
			sb.append(CommonConstants.SCREENSHOTFILEEXTENSION);
			File source = ((TakesScreenshot) TestBase.getDriver()).getScreenshotAs(OutputType.FILE);
			LOGGER.debug("screenshot file: " + source + ", path -->" + sb.toString());

			FileUtils.copyFile(source, new File(sb.toString()));
		} catch (Exception ioException) {
			LOGGER.error("Utility.takeScreenShot - IOException occurred: ", ioException);
		}
		return sb.toString();
	}

	public static final String takeScreenShot(String journeyName) {

		StringBuilder sb = new StringBuilder();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(CommonConstants.SCREENSHOTFORMAT);

//			String str = fetchHomeDir().replace("\\", "/");
//			sb.append(str);
//			sb.append("/screenshots/");
			sb.append("C:/screenshots/");
			sb.append(journeyName);
			sb.append("_");
			sb.append(sdf.format(Utility.fetchCurrentDate()));
			sb.append(CommonConstants.SCREENSHOTFILEEXTENSION);
			File source = ((TakesScreenshot) TestBase.getDriver()).getScreenshotAs(OutputType.FILE);
			LOGGER.debug("screenshot file: " + source + ", path -->" + sb.toString());

			FileUtils.copyFile(source, new File(sb.toString()));
		} catch (Exception ioException) {
			LOGGER.error("Utility.takeScreenShot - IOException occurred: ", ioException);
		}
		return sb.toString();
	}

	public final static String fetchHomeDir() {

		return System.getProperty("user.dir");
	}

	/**
	 * This method returns the current date of the system.
	 * 
	 * @return date
	 */
	public final static Date fetchCurrentDate() {

		return new Date();
	}

	/**
	 * This method returns true if the directory structure is like Windows style.
	 * Else it returns false.
	 * 
	 * @return true if the directory structure is like Windows style. Else return
	 *         false.
	 */
	public final static boolean isWindowsStylePath() {

		if (System.getProperty("file.separator").contains("\\")) {
			return true;
		}
		return false;
	}

	public static boolean isWebElementListSorted(List<WebElement> webElementList) {

		return isStringListSorted(getElementsData(webElementList));
	}

	public static boolean isWebElementListReverseSorted(List<WebElement> webElementList) {

		return isStringListReverseSorted(getElementsData(webElementList));
	}

	public static List<String> getElementsData(List<WebElement> webElementList) {
		List<String> dataList = new ArrayList<String>();
		for (WebElement webElement : webElementList) {
			if (StringUtils.isEmpty(webElement.getText())) {
				dataList.add(webElement.getAttribute("value"));
			} else {
				dataList.add(webElement.getText());
			}
		}
		return dataList;
	}

	public static boolean isStringListSorted(List<String> originalList) {

		return checkListForSorting(originalList, true);
	}

	public static boolean isStringListReverseSorted(List<String> originalList) {

		return checkListForSorting(originalList, false);
	}

	/**
	 * 
	 * @param originalList
	 * @param ascendingSortFlag - set to true for ascending sort, false for
	 *                          descending sort
	 * @return true if list is sorted as expected, otherwise false
	 */
	private static boolean checkListForSorting(List<String> originalList, boolean ascendingSortFlag) {

		List<String> editedList = new ArrayList<String>();
		List<Date> originalDateList = new ArrayList<Date>();
		List<Date> sortedDateList = new ArrayList<Date>();
		SimpleDateFormat sdf24HourFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		SimpleDateFormat sdfDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		boolean dateFlag = doesListContainDates(originalList);
		boolean timeFlag = doesListContainTime(originalList);

		editedList = handleBlankListValues(originalList);
		try {
			if (timeFlag) {
				for (String str : editedList) {

					originalDateList.add(sdf24HourFormat.parse(str));
					sortedDateList.add(sdf24HourFormat.parse(str));
				}
			} else if (dateFlag) {
				for (String str : editedList) {

					originalDateList.add(sdfDateFormat.parse(str));
					sortedDateList.add(sdfDateFormat.parse(str));
				}
			}
		} catch (ParseException e) {
			LOGGER.error("ParseException occured: ", e);
		}

		if (ascendingSortFlag) {
			if (dateFlag) {
				sortedDateList = sortDateList(sortedDateList);
				return compareDateValuesInternally(originalDateList, sortedDateList);
			} else {
				return performSorting(editedList, true);
			}
		} else { // descending sort
			if (dateFlag) {
				sortedDateList = reverseSortDateList(sortedDateList);
				return compareDateValuesInternally(originalDateList, sortedDateList);
			} else {
				return performSorting(editedList, false);
			}
		}

		// return true;
	}

	private static boolean performSorting(List<String> editedList, boolean ascendingSortFlag) {

		List<String> newStringList = new ArrayList<String>();
		List<Integer> newIntegerList = new ArrayList<Integer>();
		List<Integer> editedIntegerList = new ArrayList<Integer>();
		boolean numericFlag = true;
		for (String str : editedList) {
			if (StringUtils.isNumeric(str)) {
				newIntegerList.add(Integer.parseInt(str));
				editedIntegerList.add(Integer.parseInt(str));
			} else {
				numericFlag = false;
				break;
			}
		}
		if (numericFlag) {
			Collections.sort(newIntegerList);
			if (!ascendingSortFlag) {
				Collections.reverse(newIntegerList);
			}
			for (Integer intValue : newIntegerList) {
				newStringList.add(intValue.toString());
			}
			// to handle leading zeros in input list
			editedList = new ArrayList<String>();
			for (Integer intValue : editedIntegerList) {
				editedList.add(intValue.toString());
			}
			return compareStringValuesInternally(editedList, newStringList);
		}
		newStringList.addAll(editedList);
		Collections.sort(newStringList);
		if (!ascendingSortFlag) {
			Collections.reverse(newStringList);
		}
		return compareStringValuesInternally(editedList, newStringList);
	}

	private static List<Date> sortDateList(List<Date> dateList) {

		Collections.sort(dateList, new Comparator<Date>() {
			public int compare(Date d1, Date d2) {
				if (d1 == null || d2 == null)
					return 0;
				return d1.compareTo(d2);
			}
		});

		return dateList;
	}

	private static List<Date> reverseSortDateList(List<Date> dateList) {

		Collections.sort(dateList, new Comparator<Date>() {
			public int compare(Date d1, Date d2) {
				if (d1 == null || d2 == null)
					return 0;
				return d2.compareTo(d1);
			}
		});

		return dateList;
	}

	private static boolean doesListContainDates(List<String> originalList) {

		boolean dateFlag = false;
		for (String str : originalList) {
			if (StringUtils.isNotBlank(str)) {
				if (str.contains("/")) {
					dateFlag = true;
					break;
				}
			}
		}
		return dateFlag;
	}

	private static boolean doesListContainTime(List<String> originalList) {

		boolean timeFlag = false;
		for (String str : originalList) {
			if (StringUtils.isNotBlank(str)) {
				if (str.contains(":")) {
					timeFlag = true;
					break;
				}
			}
		}
		return timeFlag;
	}

	private static List<String> handleBlankListValues(List<String> originalList) {

		List<String> editedList = new ArrayList<String>();
		String lastValue = "";
		for (String str : originalList) {
			if (null == str) {
				if ("" == lastValue) { // to handle 1st null value in list
					str = getFirstNonBlankValue(originalList);
				} else {
					str = lastValue;
				}
			} else {
				lastValue = str;
			}
			editedList.add(str);
		}
		return editedList;
	}

	private static String getFirstNonBlankValue(List<String> originalList) {

		for (String str : originalList) {
			if (null != str) {
				return str;
			}
		}
		return null;
	}

	private static boolean compareStringValuesInternally(List<String> editedList, List<String> newList) {
		for (int i = 0; i < editedList.size(); i++) {
			LOGGER.debug("i: " + i + ", Comparing " + editedList.get(i) + " and " + newList.get(i));
			if (!editedList.get(i).equals(newList.get(i))) {
				LOGGER.debug(
						"Mismatch values at location: " + i + " , " + editedList.get(i) + " and " + newList.get(i));
				return false;
			}
		}
		return true;
	}

	private static boolean compareDateValuesInternally(List<Date> originalDateList, List<Date> sortedDateList) {
		for (int i = 0; i < originalDateList.size(); i++) {
			LOGGER.debug("i: " + i + ", Comparing " + originalDateList.get(i) + " and " + sortedDateList.get(i));
			if (!originalDateList.get(i).equals(sortedDateList.get(i))) {
				LOGGER.debug("Mismatch values at location: " + i + " , " + originalDateList.get(i) + " and "
						+ sortedDateList.get(i));
				return false;
			}
		}
		return true;
	}

	public static void flash(WebElement element) {
		String bgColor = element.getCssValue("backgroundColor");
		JavascriptExecutor js = (JavascriptExecutor) DriverManager.getDriver();
		js.executeScript("arguments[0].style.backgroundColor='rgb(255,255,0)'", element);
		js.executeScript("arguments[0].style.backgroundColor='" + bgColor + "'", element);
	}

	public static String decodeString(String value) {
		byte[] usernameDecoded = Base64.decodeBase64(value.getBytes());
		byte[] decrypted = AESUtils.decrypt(CommonConstants.SECRETKEYSTRING, usernameDecoded);
		return new String(decrypted);

	}

}
